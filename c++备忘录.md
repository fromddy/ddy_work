<!-- TITLE: c++备忘录 -->
# C++
written by river river@vvl.me  

edited by @fromddy ddy_mainland@163.com

## class
class-keyword
* class
* struct
* union

construction
  * default constructor(默认构造)
  * copy constructor(复制构造)
  *  move constructor(移动构造)
  * destructor

[1] https://zh.cppreference.com/w/cpp/language/default_constructor  
[2] https://zh.cppreference.com/w/cpp/language/copy_constructor  
[3] https://zh.cppreference.com/w/cpp/language/move_constructor  
[4] https://zh.cppreference.com/w/cpp/language/destructor



# STL
components
* Sequence containers  
   - array: static contiguous array
   - vector: dynamic contiguous array
   - deque: double-ended queue
   - forward_list: singly-linked list
   - list: doubly-linked list
* Container adaptors
   - stack: LIFO stack
   - queue: FIFO queue
   - priority_queue: priority queue
* Associative containers
    - set: collection of unique keys, sorted by keys
    - multiset: collection of keys, sorted by keys
    - map: collection of key-value pairs, sorted by keys, keys ar unique
    - multimap: collection of key-value pairs, sorted by keys
*  Unordered associative containers
    - unordered_set: collection of unique keys, hashed by keys
    - unordered_multiset: collection of key-value pairs, hashed by keys, keys are unique
    - unordered_map: collection of keys, hashed by keys
    - unordered_multimap: collection of key-value pairs, hashed by keys
* iterators
* algorithms
  - Non-modifying sequence operations
  - Modifying sequence operations
  - Partitioning operations
  - Sorting operations
  - Binary search operations (on sorted ranges)
  - Other operations on sorted ranges
  - Set operations (on sorted ranges)
  - Heap operations
  - Minimum/Maximum operations
  - Comparison operations
  - Permutation operations
  - Numeric operations
  - Operations on uninitialized memory
* functions

[1]https://en.wikipedia.org/wiki/Standard_Template_Library  
[2]https://en.wikipedia.org/wiki/Collection_(abstract_data_type)  
[3]https://en.cppreference.com/w/cpp/container  
[4]http://www.cplusplus.com/reference/stl/  
[5]https://en.wikipedia.org/wiki/Iterator  
[6]https://en.cppreference.com/w/cpp/iterator/iterator  
[7]http://www.cplusplus.com/reference/iterator/  
[8]https://en.cppreference.com/w/cpp/algorithm  
[9]http://www.cplusplus.com/reference/algorithm/  